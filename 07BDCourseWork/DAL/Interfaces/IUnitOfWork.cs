﻿using System;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Car> Cars { get; }

        IRepository<CarRepair> CarRepairs { get; }

        IRepository<Brigade> Brigades { get; }

        IRepository<Defect> Defects { get; }

        IRepository<Employee> Employees { get; }

        IRepository<SparePart> SpareParts { get; }

        IRepository<Workshop> Workshops { get; }

        void Save();
    }
}