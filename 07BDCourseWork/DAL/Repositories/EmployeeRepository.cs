﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private DbModel db;

        public EmployeeRepository(DbModel context)
        {
            db = context;
        }

        public void Create(Employee entity)
        {
            db.Employee.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.Employee.Find(id);
            if (entity != null)
            {
                db.Employee.Remove(entity);
            }
        }

        public IEnumerable<Employee> Find(Func<Employee, bool> predicate)
        {
            return db.Employee.Where(predicate).ToList();
        }

        public Employee Get(int id)
        {
            return db.Employee.Find(id);
        }

        public IEnumerable<Employee> GetAll()
        {
            return db.Employee;
        }

        public void Update(Employee entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
