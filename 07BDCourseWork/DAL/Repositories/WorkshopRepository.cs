﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class WorkshopRepository : IRepository<Workshop>
    {
        private DbModel db;

        public WorkshopRepository(DbModel context)
        {
            db = context;
        }

        public void Create(Workshop entity)
        {
            db.Workshop.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.Workshop.Find(id);
            if (entity != null)
            {
                db.Workshop.Remove(entity);
            }
        }

        public IEnumerable<Workshop> Find(Func<Workshop, bool> predicate)
        {
            return db.Workshop.Where(predicate).ToList();
        }

        public Workshop Get(int id)
        {
            return db.Workshop.Find(id);
        }

        public IEnumerable<Workshop> GetAll()
        {
            return db.Workshop;
        }

        public void Update(Workshop entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
