﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly DbModel db;
        private CarRepository carRepository;
        private CarRepairRepository carRepairRepository;
        private DefectRepository defectRepository;
        private EmployeeRepository employeeRepository;
        private SparePartRepository sparePartRepository;
        private WorkshopRepository workshopRepository;
        private BrigadeRepository brigadeRepository;
        private bool disposed;
        private IUnitOfWork unitOfWorkImplementation;

        public DbModel Context => db;

        public EFUnitOfWork(string connectionString)
        {
            db = new DbModel(connectionString);
        }

        public IRepository<Brigade> Brigades
        {
            get
            {
                if (brigadeRepository == null)
                {
                    brigadeRepository = new BrigadeRepository(db);
                }

                return brigadeRepository;
            }
        }

        public IRepository<CarRepair> CarRepairs
        {
            get
            {
                if (carRepairRepository == null)
                {
                    carRepairRepository = new CarRepairRepository(db);
                }

                return carRepairRepository;
            }
        }

        public IRepository<Car> Cars
        {
            get
            {
                if (carRepository == null)
                {
                    carRepository = new CarRepository(db);
                }

                return carRepository;
            }
        }

        public IRepository<Defect> Defects
        {
            get
            {
                if (defectRepository == null)
                {
                    defectRepository = new DefectRepository(db);
                }

                return defectRepository;
            }
        }

        public IRepository<Employee> Employees
        {
            get
            {
                if (employeeRepository == null)
                {
                    employeeRepository = new EmployeeRepository(db);
                }

                return employeeRepository;
            }
        }

        public IRepository<SparePart> SpareParts
        {
            get
            {
                if (sparePartRepository == null)
                {
                    sparePartRepository = new SparePartRepository(db);
                }

                return sparePartRepository;
            }
        }

        public IRepository<Workshop> Workshops
        {
            get
            {
                if (workshopRepository == null)
                {
                    workshopRepository = new WorkshopRepository(db);
                }

                return workshopRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
