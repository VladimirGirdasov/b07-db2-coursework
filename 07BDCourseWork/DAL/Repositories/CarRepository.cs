﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class CarRepository : IRepository<Car>
    {
        private DbModel db;

        public CarRepository(DbModel context)
        {
            db = context;
        }

        public void Create(Car entity)
        {
            db.Car.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.Car.Find(id);
            if (entity != null)
            {
                db.Car.Remove(entity);
            }
        }

        public IEnumerable<Car> Find(Func<Car, bool> predicate)
        {
            return db.Car.Where(predicate).ToList();
        }

        public Car Get(int id)
        {
            return db.Car.Find(id);
        }

        public IEnumerable<Car> GetAll()
        {
            return db.Car;
        }

        public void Update(Car entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
