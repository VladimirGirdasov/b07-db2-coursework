﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class DefectRepository : IRepository<Defect>
    {
        private DbModel db;

        public DefectRepository(DbModel context)
        {
            db = context;
        }

        public void Create(Defect entity)
        {
            db.Defect.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.Defect.Find(id);
            if (entity != null)
            {
                db.Defect.Remove(entity);
            }
        }

        public IEnumerable<Defect> Find(Func<Defect, bool> predicate)
        {
            return db.Defect.Where(predicate).ToList();
        }

        public Defect Get(int id)
        {
            return db.Defect.Find(id);
        }

        public IEnumerable<Defect> GetAll()
        {
            return db.Defect;
        }

        public void Update(Defect entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
