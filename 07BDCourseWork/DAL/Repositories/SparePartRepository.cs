﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class SparePartRepository : IRepository<SparePart>
    {
        private DbModel db;

        public SparePartRepository(DbModel context)
        {
            db = context;
        }
        public void Create(SparePart entity)
        {
            db.SparePart.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.SparePart.Find(id);
            if (entity != null)
            {
                db.SparePart.Remove(entity);
            }
        }

        public IEnumerable<SparePart> Find(Func<SparePart, bool> predicate)
        {
            return db.SparePart.Where(predicate).ToList();
        }

        public SparePart Get(int id)
        {
            return db.SparePart.Find(id);
        }

        public IEnumerable<SparePart> GetAll()
        {
            return db.SparePart;
        }

        public void Update(SparePart entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}