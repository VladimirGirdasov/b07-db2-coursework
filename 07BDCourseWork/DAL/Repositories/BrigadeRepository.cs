﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class BrigadeRepository : IRepository<Brigade>
    {
        private DbModel db;

        public BrigadeRepository(DbModel context)
        {
            db = context;
        }

        public void Create(Brigade entity)
        {
            db.Brigade.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.Brigade.Find(id);
            if (entity != null)
            {
                db.Brigade.Remove(entity);
            }
        }

        public IEnumerable<Brigade> Find(Func<Brigade, bool> predicate)
        {
            return db.Brigade.Where(predicate).ToList();
        }

        public Brigade Get(int id)
        {
            return db.Brigade.Find(id);
        }

        public IEnumerable<Brigade> GetAll()
        {
            return db.Brigade;
        }

        public void Update(Brigade entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}