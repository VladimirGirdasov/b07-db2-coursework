﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class CarRepairRepository : IRepository<CarRepair>
    {
        private DbModel db;

        public CarRepairRepository(DbModel context)
        {
            db = context;
        }

        public void Create(CarRepair entity)
        {
            db.CarRepair.Add(entity);
        }

        public void Delete(int id)
        {
            var entity = db.CarRepair.Find(id);
            if (entity != null)
            {
                db.CarRepair.Remove(entity);
            }
        }

        public IEnumerable<CarRepair> Find(Func<CarRepair, bool> predicate)
        {
            return db.CarRepair.Where(predicate).ToList();
        }

        public CarRepair Get(int id)
        {
            return db.CarRepair.Find(id);
        }

        public IEnumerable<CarRepair> GetAll()
        {
            return db.CarRepair;
        }

        public void Update(CarRepair entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}