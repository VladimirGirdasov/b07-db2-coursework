namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cost_Is_Money : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Defect", "Cost", c => c.Decimal(nullable: false, storeType: "money"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Defect", "Cost", c => c.Decimal(nullable: false, precision: 18, scale: 0));
        }
    }
}
