namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CostPrice_Is_Money : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SparePart", "Price", c => c.Decimal(nullable: false, storeType: "money"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SparePart", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 0));
        }
    }
}
