namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration_v1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brigade",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CarRepair",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IdCar = c.Int(nullable: false),
                        IdDefect = c.Int(nullable: false),
                        DateIn = c.DateTime(nullable: false),
                        DateOut = c.DateTime(),
                        IdBrigade = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Car", t => t.IdCar)
                .ForeignKey("dbo.Defect", t => t.IdDefect)
                .ForeignKey("dbo.Brigade", t => t.IdBrigade)
                .Index(t => t.IdCar)
                .Index(t => t.IdDefect)
                .Index(t => t.IdBrigade);
            
            CreateTable(
                "dbo.Car",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IdCarcase = c.Int(nullable: false),
                        IdEngine = c.Int(nullable: false),
                        Owner = c.String(maxLength: 50, unicode: false),
                        FactoryNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SparePart",
                c => new
                    {
                        IdCar = c.Int(nullable: false),
                        IdDefect = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 0),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdCar, t.IdDefect })
                .ForeignKey("dbo.Defect", t => t.IdDefect)
                .ForeignKey("dbo.Car", t => t.IdCar)
                .Index(t => t.IdCar)
                .Index(t => t.IdDefect);
            
            CreateTable(
                "dbo.Defect",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 0),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        InnEmployee = c.Int(nullable: false),
                        IdWorkshop = c.Int(nullable: false),
                        IdBrigade = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.InnEmployee)
                .ForeignKey("dbo.Workshop", t => t.IdWorkshop)
                .ForeignKey("dbo.Brigade", t => t.IdBrigade)
                .Index(t => t.IdWorkshop)
                .Index(t => t.IdBrigade);
            
            CreateTable(
                "dbo.Workshop",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employee", "IdBrigade", "dbo.Brigade");
            DropForeignKey("dbo.Employee", "IdWorkshop", "dbo.Workshop");
            DropForeignKey("dbo.CarRepair", "IdBrigade", "dbo.Brigade");
            DropForeignKey("dbo.SparePart", "IdCar", "dbo.Car");
            DropForeignKey("dbo.SparePart", "IdDefect", "dbo.Defect");
            DropForeignKey("dbo.CarRepair", "IdDefect", "dbo.Defect");
            DropForeignKey("dbo.CarRepair", "IdCar", "dbo.Car");
            DropIndex("dbo.Employee", new[] { "IdBrigade" });
            DropIndex("dbo.Employee", new[] { "IdWorkshop" });
            DropIndex("dbo.SparePart", new[] { "IdDefect" });
            DropIndex("dbo.SparePart", new[] { "IdCar" });
            DropIndex("dbo.CarRepair", new[] { "IdBrigade" });
            DropIndex("dbo.CarRepair", new[] { "IdDefect" });
            DropIndex("dbo.CarRepair", new[] { "IdCar" });
            DropTable("dbo.Workshop");
            DropTable("dbo.Employee");
            DropTable("dbo.Defect");
            DropTable("dbo.SparePart");
            DropTable("dbo.Car");
            DropTable("dbo.CarRepair");
            DropTable("dbo.Brigade");
        }
    }
}
