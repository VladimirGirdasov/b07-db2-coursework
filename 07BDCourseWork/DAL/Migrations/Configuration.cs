using System.Collections.Generic;
using System.Data;
using DAL.Entities;
using DAL.Repositories;

namespace DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DAL.Entities.DbModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DAL.Entities.DbModel context)
        {
            // As Entity Framework cant create Uniq constraints, triggers, etc - lets do it with RAW SQL
            var DbUnitOfWork = new EFUnitOfWork("name=DbModel");

            #region Constraints UNIQUE

            // Brigade table Uniq constraint name
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("ALTER TABLE Brigade ADD CONSTRAINT Constraint_Brigade_Uniq_Name UNIQUE (Name);");

            // Workshop table Uniq constraint name
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE Workshop ADD CONSTRAINT Constraint_Workshop_Uniq_Name UNIQUE (Name);");

            // Car table Uniq constraint IdCarcase
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE Car ADD CONSTRAINT Constraint_Workshop_Uniq_IdCarcase UNIQUE (IdCarcase);");

            // Car table Uniq constraint IdEngine                        
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE Car ADD CONSTRAINT Constraint_Workshop_Uniq_IdEngine UNIQUE (IdEngine);");

            // Car table Uniq constraint FactoryNumber                   
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE Car ADD CONSTRAINT Constraint_Workshop_Uniq_FactoryNumber UNIQUE (FactoryNumber);");

            #endregion

            #region Constraints DEFAULT

            // CarRepair table Default constraint DateIn
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE CarRepair ADD CONSTRAINT Constraint_CarRepair_Default_DateIn DEFAULT GETDATE() FOR DateIn;");

            #endregion

            #region Constraints CHECK

            // SparePart table CHECK constraint Amount > 0 Price >= 0.01
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE SparePart ADD CONSTRAINT chk_SpareParts CHECK(Amount > 0 AND Price >= CONVERT(decimal, 0.01));");

            // Defect table CHECK Cost >= 0.01
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(
                "ALTER TABLE Defect ADD CONSTRAINT chk_Defect CHECK(Cost >= CONVERT(decimal, 0.01));");

            #endregion

            #region INDEX

            // Workshop uniuq name
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX Indx_WorkshopUniqName ON Workshop(Name);");

            // Employee name
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_EmployeeName ON Employee(Name);");

            // Brigade uniuq name
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX Indx_BrigadeUniqName ON Brigade(Name);");

            // CarRepair IdCar
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_CarRepairIdCar ON CarRepair(IdCar);");

            // CarRepair IdDefect
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_CarRepairIdDefect ON CarRepair(IdDefect);");

            // CarRepair DateIn
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_CarRepairDateIn ON CarRepair(DateIn);");

            // CarRepair DateOut
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_CarRepairDateOut ON CarRepair(DateOut);");

            // Car Owner
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_CarOwner ON Car(Owner);");

            // Defect Cost
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_CarDefectCost ON Defect(Cost);");

            // SparePart IdCar
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_SparePartIdCar ON SparePart(IdCar);");

            // SparePart IdDefect
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_SparePartIdDefect ON SparePart(IdDefect);");

            // SparePart Price
            DbUnitOfWork.Context.Database.ExecuteSqlCommand("CREATE INDEX Indx_SparePartPrice ON SparePart(Price);");

            #endregion

            #region Triggers

            DbUnitOfWork.Context.Database.ExecuteSqlCommand(@"CREATE TRIGGER Car_DeleteRelatives 
                                                            ON Car INSTEAD OF DELETE 
                                                            AS 
                                                            BEGIN 
                                                            SET NOCOUNT ON; 
                                                            DECLARE @CarId INT 
                                                            SELECT @CarId = DELETED.ID FROM DELETED 
                                                            DELETE FROM SparePart WHERE  SparePart.idCar = @CarId; 
                                                            DELETE FROM CarRepair WHERE  CarRepair.idCar = @CarId; 
                                                            DELETE FROM Car WHERE Car.ID = @CarId; END");

            #endregion

            #region Views

            // ������ id ��� ��������� �����, ������� ������� �� ��� ���, � ����� ������� ���������� ���� ������� �� ��� ��� ����
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(@"/*
                                                            1 ����. �� ���. GROUP � HAVING
                                                            ������: id ��� ��������� �����, ������� ������� �� ��� ���, � ����� ������� ���������� ���� ������� �� ��� ��� ����
                                                            */

                                                            CREATE VIEW View_1
                                                            AS
                                                            SELECT ID, DATEDIFF(DAY,DateIn,GETDATE()) AS 'Days left' 
                                                            FROM CarRepair
                                                            WHERE DateOut IS NULL");

            // ������ id �����������, �� ������ ��������� ������������� ������� ������� ��� ������� ���� ���������
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(@"/*
                                                            1 ����. ���. GROUP � HAVING
                                                            ������: id �����������, �� ������ ��������� ������� ������� ������� ��� ������� ���� ���������
                                                            */

                                                            CREATE VIEW View_2
                                                            AS
                                                            SELECT IdCar AS 'ID of car which took more than 1 spares in single repairing'
                                                            FROM SparePart
                                                            GROUP BY IdCar
                                                            HAVING MIN(Amount) > 1");

            // ������: ����������� ������������, ���������� ��������� ����������� �� ���� � ������� ���� ��������, ��� �������, ��� ������� ���� ������ ���� ������ 100
            DbUnitOfWork.Context.Database.ExecuteSqlCommand(@"/*
                                                             2 ����. ���. GROUP � HAVING
                                                             ������: ����������� ������������, ���������� ��������� ����������� �� ���� � ������� ���� ��������,
                                                             ��� �������, ��� ������� ���� ������ ���� ������ 100
                                                              */

                                                            CREATE VIEW View_3
                                                            AS
                                                            SELECT c.Owner, COUNT(sp.IdCar) AS 'Count of spare parts', AVG(sp.Price) AS 'Average price'
                                                            FROM Car c
                                                            JOIN SparePart sp
                                                            ON c.ID = sp.IdCar
                                                            GROUP BY c.Owner
                                                            HAVING AVG(sp.Price) > 100");

            #endregion

            DbUnitOfWork.Save();

            //CAR

            var cars = new List<Car>
            {
                new Car
                {
                    FactoryNumber = 5407097,
                    IdCarcase = 994692453,
                    IdEngine = 3812583,
                    Owner = "Dmitrii Kolosov"
                },
                new Car
                {
                    FactoryNumber = 740230971,
                    IdCarcase = 64692453,
                    IdEngine = 3846183,
                    Owner = "Mihail Shudegov"
                },
                new Car
                {
                    FactoryNumber = 236371,
                    IdCarcase = 56342,
                    IdEngine = 384213,
                    Owner = "Andrey Yatsenko"
                }
            };

            foreach (var car in cars)
            {
                context.Car.Add(car);
            }

            //DEFECT

            var defects = new List<Defect>
            {
                new Defect
                {
                    Cost = 30.00m,
                    Name = "Broken mirror"
                },
                new Defect
                {
                    Cost = 120.00m,
                    Name = "No Wheels"
                },
                new Defect
                {
                    Cost = 450.00m,
                    Name = "Engine destroyed"
                }
            };

            foreach (var item in defects)
            {
                context.Defect.Add(item);
            }

            //WORKSHOP

            var workshops = new List<Workshop>
            {
                new Workshop
                {
                    Name = "Workshop on Lenina Street"
                },
                new Workshop
                {
                    Name = "Workshop on Pushkinskaya Street"
                },
                new Workshop
                {
                    Name = "New workshop in Cetral Square"
                },
                new Workshop
                {
                    Name = "Workshop on Krasnoarmeysaya Street"
                }
            };

            foreach (var item in workshops)
            {
                context.Workshop.Add(item);
            }

            context.SaveChanges();

            //BRIAGDE

            var brigades = new List<Brigade>
            {
                new Brigade
                {
                    Name = "Brigade #3"
                },
                new Brigade
                {
                    Name = "Brigade #12"
                },
                new Brigade
                {
                    Name = "Brigade #7"
                },
                new Brigade
                {
                    Name = "Brigade of mounth!"
                }
            };

            foreach (var item in brigades)
            {
                context.Brigade.Add(item);
            }

            context.SaveChanges();

            //SPARE PARTS

            var spareParts = new List<SparePart>
            {
                new SparePart
                {
                    Amount = 1,
                    Car = cars[0],
                    Defect = defects[0],
                    Name = "Mirror 'MirrorName'",
                    Price = 12.10m
                },
                new SparePart
                {
                    Amount = 4,
                    Car = cars[1],
                    Defect = defects[1],
                    Name = "Wheels 'Yokohama'",
                    Price = 460.50m
                },
                new SparePart
                {
                    Amount = 2,
                    Car = cars[1],
                    Defect = defects[0],
                    Name = "Mirror 'Brand'",
                    Price = 50.70m
                },
                new SparePart
                {
                    Amount = 1,
                    Car = cars[2],
                    Defect = defects[2],
                    Name = "Engine Yamaha 450",
                    Price = 1250.00m
                },
            };

            foreach (var item in spareParts)
            {
                context.SparePart.Add(item);
            }

            //CAR REPAIR

            var repairs = new List<CarRepair>
            {
                new CarRepair
                {
                    Car = cars[0],
                    Defect = defects[0],
                    DateIn = new DateTime(year: 2005, month: 10, day: 2),
                    DateOut = new DateTime(year: 2005, month: 12, day: 1),
                    Brigade = brigades[0]
                },
                new CarRepair
                {
                    Car = cars[1],
                    Defect = defects[1],
                    DateIn = new DateTime(year: 2009, month: 1, day: 12),
                    DateOut = new DateTime(year: 2009, month: 12, day: 2),
                    Brigade = brigades[1]
                },
                new CarRepair
                {
                    Car = cars[1],
                    Defect = defects[0],
                    DateIn = new DateTime(year: 2007, month: 11, day: 1),
                    DateOut = new DateTime(year: 2009, month: 2, day: 2),
                    Brigade = brigades[3]
                },
                new CarRepair
                {
                    Car = cars[2],
                    Defect = defects[2],
                    DateIn = new DateTime(year: 2014, month: 11, day: 19),
                    DateOut = new DateTime(year: 2014, month: 12, day: 13),
                    Brigade = brigades[3]
                },
                new CarRepair
                {
                    Car = cars[0],
                    Defect = defects[0],
                    DateIn = new DateTime(year: 2006, month: 11, day: 3),
                    DateOut = new DateTime(year: 2006, month: 12, day: 1),
                    Brigade = brigades[0]
                }
            };

            foreach (var item in repairs)
            {
                context.CarRepair.Add(item);
            }

            //EMPLOYEES

            var employees = new List<Employee>
            {
                new Employee
                {
                    Brigade = brigades[0],
                    Workshop = workshops[0],
                    InnEmployee = 144,
                    Name = "Kirill Vasiliev"
                },
                new Employee
                {
                    Brigade = brigades[0],
                    Workshop = workshops[0],
                    InnEmployee = 1442,
                    Name = "Dmitrii Chizhov"
                },
                new Employee
                {
                    Brigade = brigades[1],
                    Workshop = workshops[1],
                    InnEmployee = 1462,
                    Name = "Alexei Redkin"
                },
                new Employee
                {
                    Brigade = brigades[3],
                    Workshop = workshops[2],
                    InnEmployee = 6122,
                    Name = "Artem Pozdeev"
                },
                new Employee
                {
                    Brigade = brigades[3],
                    Workshop = workshops[2],
                    InnEmployee = 451112,
                    Name = "Ekaterina Ivanova"
                },
                new Employee
                {
                    Brigade = brigades[3],
                    Workshop = workshops[2],
                    InnEmployee = 65424,
                    Name = "Bylichev Egor"
                }
            };

            foreach (var item in employees)
            {
                context.Employee.Add(item);
            }

            context.SaveChanges();
        }
    }
}