namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Car")]
    public partial class Car
    {
        public Car()
        {
            CarRepair = new HashSet<CarRepair>();
            SparePart = new HashSet<SparePart>();
        }

        public int ID { get; set; }

        public int IdCarcase { get; set; }

        public int IdEngine { get; set; }

        [StringLength(50)]
        public string Owner { get; set; }

        public int FactoryNumber { get; set; }

        public virtual ICollection<CarRepair> CarRepair { get; set; }

        public virtual ICollection<SparePart> SparePart { get; set; }
    }
}
