namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Brigade")]
    public partial class Brigade
    {
        public Brigade()
        {
            CarRepair = new HashSet<CarRepair>();
            Employee = new HashSet<Employee>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<CarRepair> CarRepair { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
    }
}
