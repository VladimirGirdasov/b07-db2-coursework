namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InnEmployee { get; set; }

        public int IdWorkshop { get; set; }

        public int IdBrigade { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual Brigade Brigade { get; set; }

        public virtual Workshop Workshop { get; set; }
    }
}
