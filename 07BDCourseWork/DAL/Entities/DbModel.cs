namespace DAL.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DbModel : DbContext
    {
        public DbModel(string connectionString)
            : base(connectionString)
        {
        }

        public DbModel()
            : base("name=DbModel")
        {
        }

        public virtual DbSet<Brigade> Brigade { get; set; }
        public virtual DbSet<Car> Car { get; set; }
        public virtual DbSet<CarRepair> CarRepair { get; set; }
        public virtual DbSet<Defect> Defect { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<SparePart> SparePart { get; set; }
        public virtual DbSet<Workshop> Workshop { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brigade>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Brigade>()
                .HasMany(e => e.CarRepair)
                .WithOptional(e => e.Brigade)
                .HasForeignKey(e => e.IdBrigade);

            modelBuilder.Entity<Brigade>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Brigade)
                .HasForeignKey(e => e.IdBrigade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Car>()
                .Property(e => e.Owner)
                .IsUnicode(false);

            modelBuilder.Entity<Car>()
                .HasMany(e => e.CarRepair)
                .WithRequired(e => e.Car)
                .HasForeignKey(e => e.IdCar)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Car>()
                .HasMany(e => e.SparePart)
                .WithRequired(e => e.Car)
                .HasForeignKey(e => e.IdCar)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Defect>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Defect>()
                .Property(e => e.Cost)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Defect>()
                .HasMany(e => e.CarRepair)
                .WithRequired(e => e.Defect)
                .HasForeignKey(e => e.IdDefect)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Defect>()
                .HasMany(e => e.SparePart)
                .WithRequired(e => e.Defect)
                .HasForeignKey(e => e.IdDefect)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SparePart>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SparePart>()
                .Property(e => e.Price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Workshop>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Workshop>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Workshop)
                .HasForeignKey(e => e.IdWorkshop)
                .WillCascadeOnDelete(false);
        }
    }
}
