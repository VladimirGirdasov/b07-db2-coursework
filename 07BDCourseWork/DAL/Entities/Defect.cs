namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Defect")]
    public partial class Defect
    {
        public Defect()
        {
            CarRepair = new HashSet<CarRepair>();
            SparePart = new HashSet<SparePart>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        public decimal Cost { get; set; }

        public virtual ICollection<CarRepair> CarRepair { get; set; }

        public virtual ICollection<SparePart> SparePart { get; set; }
    }
}
