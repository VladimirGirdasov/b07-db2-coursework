namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CarRepair")]
    public partial class CarRepair
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int IdCar { get; set; }

        [Required]
        public int IdDefect { get; set; }

        public DateTime DateIn { get; set; }

        public DateTime? DateOut { get; set; }

        public int? IdBrigade { get; set; }

        public virtual Brigade Brigade { get; set; }

        public virtual Car Car { get; set; }

        public virtual Defect Defect { get; set; }
    }
}
