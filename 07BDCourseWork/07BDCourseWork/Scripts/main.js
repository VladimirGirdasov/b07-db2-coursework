﻿;

// moving Header
$("#mainNav")
    .affix({
        offset: {
            top: 100
        }
    });

// Bootstrap tooltip
$(document)
    .ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

// Body under fixed header with dynamic height
var divHeight = $("#mainNav").height();
$("#mainBody").css("margin-top", divHeight + "px");

$(window)
    .resize(function() {
        const divHeight = $("#mainNav").height();
        $("#mainBody").css("margin-top", divHeight + "px");
    });

// REPORT 1 COMPOSE click
$(function() {
    $("#btn-report-cost-repair")
        .click(function() {
            $.ajax({
                    type: "GET",
                    url: "/Report/ReportCostOfAllRepairForEachCustomer",
                    beforeSend: function() {
                        $("#ajax-report-cost-repair")
                            .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                    }
                })
                .success(function(responseData) {
                    $("#ajax-report-cost-repair").html(responseData);
                });
        });
});

// REPORT 2 COMPOSE click
$(function() {
    $("#btn-report-productivity-of-brigades")
        .click(function() {
            $.ajax({
                    type: "GET",
                    url: "/Report/ReportProductivityOfBrigadesBeetweenDates",
                    beforeSend: function () {
                        $("#ajax-report-productivity-of-brigades")
                            .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                    }
                })
                .success(function(responseData) {
                    $("#ajax-report-productivity-of-brigades").html(responseData);
                });
        });
});

// REPORT 3 COMPOSE click
$(function () {
    $("#btn-report-defects")
        .click(function () {
            $.ajax({
                type: "GET",
                url: "/Report/ReportDefects",
                beforeSend: function () {
                    $("#ajax-report-defects")
                        .html("<img alt=\"loading.gif\" src=\"../Content/light_blue_material_design_loading.gif\" />");
                }
            })
                .success(function (responseData) {
                    $("#ajax-report-defects").html(responseData);
                });
        });
});