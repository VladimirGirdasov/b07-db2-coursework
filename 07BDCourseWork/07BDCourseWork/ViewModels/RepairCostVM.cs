﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _07BDCourseWork.ViewModels
{
    public class RepairCostVM
    {
        [DisplayName(@"Имя заказчика")]
        public string Owner { get; set; }

        [DisplayName(@"Стоимость починки неисправностей")]
        [DataType(DataType.Currency)]
        public decimal DefectRepairingCost { get; set; }

        [DisplayName(@"Стоимость затраченных запчастей")]
        [DataType(DataType.Currency)]
        public decimal SparePartCost { get; set; }

        [DisplayName(@"Всего")]
        [DataType(DataType.Currency)]
        public decimal Total { get; set; }
    }
}