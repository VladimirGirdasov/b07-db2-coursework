﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Entities;
using _07BDCourseWork.Models;

namespace _07BDCourseWork.ViewModels
{
    public class SparePartCreateEdit : SparePartDTO
    {
        public SparePartCreateEdit(List<CarDTO> selectListCar, List<DefectDTO> selectListDefect)
        {
            SelectListCar = selectListCar;
            SelectListDefect = selectListDefect;
        }

        public SparePartCreateEdit()
        {
            
        }

        public List<CarDTO> SelectListCar { get; set; }

        public List<DefectDTO> SelectListDefect { get; set; }
    }
}