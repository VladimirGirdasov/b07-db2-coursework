﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DAL.Entities;
using _07BDCourseWork.Models;

namespace _07BDCourseWork.ViewModels
{
    public class EmployeeCreateEdit : EmployeeDTO
    {
        public EmployeeCreateEdit(List<WorkshopDTO> workshops, List<BrigadeDTO> brigades)
        {
            SelectListWorkshop = workshops;
            SelectListBrigade = brigades;
        }

        public EmployeeCreateEdit()
        {
            
        }

        public List<WorkshopDTO> SelectListWorkshop { get; set; }

        public List<BrigadeDTO> SelectListBrigade { get; set; }
    }
}