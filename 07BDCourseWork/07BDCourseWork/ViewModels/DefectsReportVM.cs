﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _07BDCourseWork.ViewModels
{
    public class DefectsReportVM
    {
        [DisplayName(@"Неисправность")]
        public string Name { get; set; }

        [DisplayName(@"Средняя стоимость починки")]
        [DataType(DataType.Currency)]
        public decimal AvgCost { get; set; }

        [DisplayName(@"Количество обращений")]
        public int TimesApplied { get; set; }
    }
}