﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _07BDCourseWork.ViewModels
{
    public class BrigadeProductivityVM
    {
        [DisplayName(@"Название бригады")]
        public string Name { get; set; }

        [DisplayName(@"Заработано с починки неисправностей")]
        [DataType(DataType.Currency)]
        public decimal DefectRepairingCost { get; set; }

        [DisplayName(@"Заработано с проданных запчастей")]
        [DataType(DataType.Currency)]
        public decimal SparePartCost { get; set; }

        [DisplayName(@"Всего")]
        [DataType(DataType.Currency)]
        public decimal Total { get; set; }
    }
}