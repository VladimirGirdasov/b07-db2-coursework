﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _07BDCourseWork.Models;

namespace _07BDCourseWork.ViewModels
{
    public class CarRepairCreateEdit : CarRepairDTO
    {
        public CarRepairCreateEdit(List<CarDTO> cars, List<DefectDTO> defects, List<BrigadeDTO> brigades)
        {
            SelectListCar = cars;
            SelectListDefect = defects;
            SelectListBrigade = brigades;
        }

        public CarRepairCreateEdit()
        {
            
        }

        public List<CarDTO> SelectListCar { get; set; }

        public List<DefectDTO> SelectListDefect { get; set; }

        public List<BrigadeDTO> SelectListBrigade { get; set; }
    }
}