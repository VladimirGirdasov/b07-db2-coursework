﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using _07BDCourseWork.App_Start;

namespace _07BDCourseWork
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var bundleStyles = new StyleBundle("~/Content/MainCss");
            bundleStyles.Include("~/Content/bootstrap.min.css",
                "~/Content/font-awesome.min.css",
                "~/Content/Site.css");

            var bundleScripts = new ScriptBundle("~/Scripts/MainScripts");
            bundleScripts.Include("~/scripts/jquery-1.10.2.min.js",
                "~/scripts/bootstrap.min.js",
                "~/scripts/main.js",
                "~/scripts/jquery.unobtrusive-ajax.min.js");

            BundleTable.Bundles.Add(bundleStyles);
            BundleTable.Bundles.Add(bundleScripts);

            AutoMapperConfig.RegisterMappings();
        }
    }
}
