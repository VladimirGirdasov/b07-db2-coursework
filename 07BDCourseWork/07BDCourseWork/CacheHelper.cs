﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _07BDCourseWork.Models;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork
{
    public static class CacheHelper
    {
        public static object Get(string key)
        {
            return HttpContext.Current.Application[key];
        }

        public static void Set(object model, string key)
        {
            HttpContext.Current.Application[key] = model;
        }
    }
}