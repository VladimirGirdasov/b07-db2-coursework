﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Web.Mvc;
using DAL.Repositories;
using _07BDCourseWork.ViewModels;

namespace _07BDCourseWork.Controllers
{
    public class ReportController : Controller
    {
        private readonly EFUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportCostOfAllRepairForEachCustomer()
        {
            Thread.Sleep(200); /* I wanna show my spinner ^^ */
            return PartialView("_ReportCostRepair");
        }

        public ActionResult ReportProductivityOfBrigadesBeetweenDates()
        {
            Thread.Sleep(200); /* I wanna show my spinner ^^ */
            return PartialView("_ReportProductivityOfPrigades");
        }

        public ActionResult ReportDefects()
        {
            Thread.Sleep(200); /* I wanna show my spinner ^^ */
            return PartialView("_ReportDefects");
        }

        public ActionResult RunReportCostOfAllRepairForEachCustomer(DateTime? datemin, DateTime? datemax, string radioSortMethod)
        {
            // 1) Сформировать отчет расчитывающий прибыль от ремонтных работ для каждого клиента завершенных в пределах заданной даты (Учитывать как стоимость работы
            // , так и стоимость самих запчаестей и их количество)

            var sqlDateMin = DateTime.Now.AddYears(-200).ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (datemin != null)
                sqlDateMin = ((DateTime)datemin).ToString("yyyy-MM-dd HH:mm:ss.fff");

            var sqlDateMax = DateTime.Now.AddYears(200).ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (datemax != null)
                sqlDateMax = ((DateTime)datemax).ToString("yyyy-MM-dd HH:mm:ss.fff");

            var dt = new DataTable();

            using (var conn = (SqlConnection) db.Context.Database.Connection)
            {
                if ((conn != null) && (conn.State == ConnectionState.Closed))
                {
                    conn.Open();
                }
                using (new SqlDataAdapter())
                {
                    var com = new SqlDataAdapter(
                        $@"SELECT c.Owner
                        , SUM(d.Cost) AS 'DefectRepairingCost'
                        , SUM(sp.Amount * sp.Price) AS 'SparePartCost'
                        , SUM(d.Cost + sp.Amount * sp.Price) AS 'Total'
                        FROM Car c
                        JOIN CarRepair cr ON c.ID = cr.IdCar
                        JOIN Defect d ON d.ID = cr.IdDefect
                        LEFT JOIN SparePart sp ON sp.IdCar = c.ID AND sp.IdDefect = d.ID
                        WHERE DateOut BETWEEN '{sqlDateMin}' AND '{sqlDateMax}'
                        GROUP BY c.Owner
                        ORDER BY {radioSortMethod}"
                        , conn);
                    com.Fill(dt);
                }
            }

            var model = new List<RepairCostVM>();

            foreach (DataRow dataRow in dt.Rows)
            {
                var modelElement = new RepairCostVM
                {
                    Owner = dataRow["Owner"].ToString(),
                    DefectRepairingCost = decimal.Parse(dataRow["DefectRepairingCost"].ToString()),
                    SparePartCost = !Convert.IsDBNull(dataRow["SparePartCost"]) ? decimal.Parse(dataRow["SparePartCost"].ToString()) : 0,
                    Total = !Convert.IsDBNull(dataRow["SparePartCost"]) ? decimal.Parse(dataRow["Total"].ToString()) : decimal.Parse(dataRow["DefectRepairingCost"].ToString())
                };
                model.Add(modelElement);
            }

            if (model.Count == 0)
            {
                ViewBag.Error = "В данном промежутке времени нет информации о выполненных заказах!";
            }

            Thread.Sleep(300); /* I wanna show my spinner ^^ */
            return PartialView("_RunReportCostOfAllRepairForEachCustomer", model);
        }

        public ActionResult RunReportProductivityOfPrigades(DateTime? datemin, DateTime? datemax, string radioSortMethod)
        {
            // 2) Сформировать отчет рассчитывающий производительность труда бригад, выражающийся в суммарной прибыли за заданный промежуток времени

            var sqlDateMin = DateTime.Now.AddYears(-200).ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (datemin != null)
                sqlDateMin = ((DateTime)datemin).ToString("yyyy-MM-dd HH:mm:ss.fff");

            var sqlDateMax = DateTime.Now.AddYears(200).ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (datemax != null)
                sqlDateMax = ((DateTime)datemax).ToString("yyyy-MM-dd HH:mm:ss.fff");

            var dt = new DataTable();

            using (var conn = (SqlConnection)db.Context.Database.Connection)
            {
                if ((conn != null) && (conn.State == ConnectionState.Closed))
                {
                    conn.Open();
                }
                using (new SqlDataAdapter())
                {
                    var com = new SqlDataAdapter(
                        $@"SELECT b.Name, SUM(d.Cost) AS 'DefectRepairingCost', SUM(sp.Amount * sp.Price) AS 'SparePartCost', SUM(d.Cost + sp.Amount * sp.Price) AS 'Total'
                        FROM Brigade b
                        JOIN CarRepair cr ON cr.IdBrigade = b.ID
                        JOIN Defect d ON d.ID = cr.IdDefect
                        JOIN Car c ON c.ID = cr.IdCar 
                        LEFT JOIN SparePart sp ON sp.IdDefect = d.ID AND sp.IdCar = c.ID
                        WHERE DateOut BETWEEN '{sqlDateMin}' AND '{sqlDateMax}'
                        GROUP BY b.Name
                        ORDER BY {radioSortMethod};"
                        , conn);
                    com.Fill(dt);
                }
            }

            var model = new List<BrigadeProductivityVM>();

            foreach (DataRow dataRow in dt.Rows)
            {
                var modelElement = new BrigadeProductivityVM
                {
                    Name = dataRow["Name"].ToString(),
                    DefectRepairingCost = decimal.Parse(dataRow["DefectRepairingCost"].ToString()),
                    SparePartCost = !Convert.IsDBNull(dataRow["SparePartCost"]) ?  decimal.Parse(dataRow["SparePartCost"].ToString()) : 0,
                    Total = !Convert.IsDBNull(dataRow["SparePartCost"]) ? decimal.Parse(dataRow["Total"].ToString()) : decimal.Parse(dataRow["DefectRepairingCost"].ToString())
                };
                model.Add(modelElement);
            }

            if (model.Count == 0)
            {
                ViewBag.Error = "В данном промежутке времени нет информации о выполненных заказах!";
            }

            Thread.Sleep(300); /* I wanna show my spinner ^^ */
            return PartialView("_RunReportProductivityOfPrigades", model);
        }

        public ActionResult RunReportDefects(DateTime? datemin, DateTime? datemax, string toggle, string count)
        {
            // 3) Сформировать отчет, показывающий N самых частых/редких неисправностей в пределах заданной даты. Показать среднюю стоимость её починки

            var sqlDateMin = DateTime.Now.AddYears(-200).ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (datemin != null)
                sqlDateMin = ((DateTime)datemin).ToString("yyyy-MM-dd HH:mm:ss.fff");

            var sqlDateMax = DateTime.Now.AddYears(200).ToString("yyyy-MM-dd HH:mm:ss.fff");
            if (datemax != null)
                sqlDateMax = ((DateTime)datemax).ToString("yyyy-MM-dd HH:mm:ss.fff");

            var dt = new DataTable();

            using (var conn = (SqlConnection)db.Context.Database.Connection)
            {
                if ((conn != null) && (conn.State == ConnectionState.Closed))
                {
                    conn.Open();
                }
                using (new SqlDataAdapter())
                {
                    string sortMethod = toggle != null ? "DESC" : "ASC";

                    var com = new SqlDataAdapter(
                        $@"SELECT TOP {count} d.Name, AVG(d.Cost) AS 'AvgCost', COUNT(cr.ID) AS 'TimesApplied'
                        FROM CarRepair cr
                        JOIN Defect d ON d.ID = cr.IdDefect
                        WHERE DateIn BETWEEN '{sqlDateMin}' AND '{sqlDateMax}'
                        GROUP BY d.Name
                        ORDER BY 3 {sortMethod}"
                        , conn);
                    com.Fill(dt);
                }
            }

            var model = new List<DefectsReportVM>();

            foreach (DataRow dataRow in dt.Rows)
            {
                var modelElement = new DefectsReportVM
                {
                    Name = dataRow["Name"].ToString(),
                    AvgCost = decimal.Parse(dataRow["AvgCost"].ToString()),
                    TimesApplied = int.Parse(dataRow["TimesApplied"].ToString())
                };
                model.Add(modelElement);
            }

            if (model.Count == 0)
            {
                ViewBag.Error = "В данном промежутке времени нет информации о выполненных заказах!";
            }

            Thread.Sleep(300); /* I wanna show my spinner ^^ */
            return PartialView("_RunReportDefects", model);
        }
    }
}