﻿using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using DAL.Interfaces;
using DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Controllers
{
    public class WorkshopController : Controller
    {
        private readonly IUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: Workshop
        public ActionResult Index()
        {
            List<WorkshopDTO> model = db.Workshops.GetAll().Select(item => item.ToWorkshopDTO()).ToList();

            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.WorkshopModel) as List<WorkshopDTO>;
            model = model ?? db.Workshops.GetAll().Select(item => item.ToWorkshopDTO()).ToList();

            if (sortOrder == SortMethods.Name)
                return PartialView("_Table", model.OrderBy(x => x.Name).ToList());

            if (sortOrder == SortMethods.NameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Name).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            List<WorkshopDTO> model = db.Workshops.GetAll().Where(x=>x.Name.StartsWith(name)).Select(item => item.ToWorkshopDTO()).ToList();
            return PartialView("_Table", model);
        }

        // GET: Workshop/Details/5
        public ActionResult Details(int id)
        {
            var model = db.Workshops.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToWorkshopDTO());
        }

        // GET: Workshop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Workshop/Create
        [HttpPost]
        public ActionResult Create(WorkshopDTO model, FormCollection collection)
        {
            ValidateUniqueName(model);

            if (ModelState.IsValid)
            {
                var modelDTO = model.ToWorkshop();
                db.Workshops.Create(modelDTO);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Workshop/Edit/5
        public ActionResult Edit(int id)
        {
            var model = db.Workshops.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToWorkshopDTO());
        }

        // POST: Workshop/Edit/5
        [HttpPost]
        public ActionResult Edit(WorkshopDTO model, FormCollection collection)
        {
            var modelToUpdate = db.Workshops.Get(model.ID);

            if (modelToUpdate == null)
                return HttpNotFound();

            ValidateUniqueName(model);

            if (ModelState.IsValid)
            {
                modelToUpdate.Name = model.Name;
                db.Save();

                return RedirectToAction("Index");
            }

            return View("Edit", model);

        }

        // GET: Workshop/Delete/5
        public ActionResult Delete(int id)
        {
            var model = db.Workshops.Get(id);

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToWorkshopDTO());
        }

        // POST: Workshop/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var model = db.Workshops.Get(id);

            if (model == null)
                HttpNotFound();

            db.Workshops.Delete(id);
            db.Save();
            return RedirectToAction("Index");
        }

        private void ValidateUniqueName(WorkshopDTO model)
        {
            var existedSameName = db.Workshops.Find(x => x.Name == model.Name && x.ID != model.ID);

            if (existedSameName != null && existedSameName.Count() != 0)
            {
                ModelState.AddModelError("Name", Messages.WorkshopCreateValid_UniqName);
            }
        }
    }
}
