﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using DAL.Repositories;
using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using _07BDCourseWork.Resources;
using _07BDCourseWork.ViewModels;

namespace _07BDCourseWork.Controllers
{
    public class SparePartController : Controller
    {
        private readonly EFUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: SparePart
        public ActionResult Index()
        {
            var model = db.SpareParts.GetAll().Select(item => item.ToSparePartDTO()).ToList();

            if(model.Count != 0)
            {
                ViewBag.Min = (int)model.Min(x => x.Price) - 1;
                ViewBag.Max = (int)model.Max(x => x.Price) + 1;
            }

            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.SparePartModel) as List<SparePartDTO>;
            model = model ?? db.SpareParts.GetAll().Select(item => item.ToSparePartDTO()).ToList();

            if (sortOrder == SortMethods.IdCarcase)
                return PartialView("_Table", model.OrderBy(x => x.Car.IdCarcase).ToList());
            if (sortOrder == SortMethods.IdCarcaseDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Car.IdCarcase).ToList());
            if (sortOrder == SortMethods.Name)
                return PartialView("_Table", model.OrderBy(x => x.Name).ToList());
            if (sortOrder == SortMethods.NameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Name).ToList());
            if (sortOrder == SortMethods.DefectName)
                return PartialView("_Table", model.OrderBy(x => x.Defect.Name).ToList());
            if (sortOrder == SortMethods.DefectNameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Defect.Name).ToList());
            if (sortOrder == SortMethods.Price)
                return PartialView("_Table", model.OrderBy(x => x.Price).ToList());
            if (sortOrder == SortMethods.PriceDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Price).ToList());
            if (sortOrder == SortMethods.Amount)
                return PartialView("_Table", model.OrderBy(x => x.Amount).ToList());
            if (sortOrder == SortMethods.AmountDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Amount).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Filter(string carcase, string name, string defect, string mincost, string maxcost)
        {
            decimal min, max;

            if (!decimal.TryParse(mincost, out min) || !decimal.TryParse(maxcost, out max))
                return PartialView("_Table", new List<SparePartDTO>());

            if (min > max)
                return PartialView("_Table", new List<SparePartDTO>());

            List<SparePartDTO> model = db.SpareParts.GetAll().Select(item => item.ToSparePartDTO()).ToList();

            model = model.Where(x => x.Name.ToString().StartsWith(name)).ToList();
            model = model.Where(x => x.Car.IdCarcase.ToString().StartsWith(carcase)).ToList();
            model = model.Where(x => x.Defect.Name.ToString().StartsWith(defect)).ToList();
            model = model.Where(x => x.Price >= min).ToList();
            model = model.Where(x => x.Price <= max).ToList();

            return PartialView("_Table", model);
        }

        // GET: SparePart/Details/5
        public ActionResult Details(int idCar, int idDefect)
        {
            var model = db.SpareParts.Find(x => x.IdCar == idCar && x.IdDefect == idDefect).FirstOrDefault();

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToSparePartDTO());
        }

        // GET: SparePart/Create
        public ActionResult Create()
        {
            var modelOut = new SparePartCreateEdit(
                 db.Cars.GetAll().Select(x => x.ToCarDTO()).ToList(),
                db.Defects.GetAll().Select(x => x.ToDefectDTO()).ToList());

            return View("Create", modelOut);
        }

        public ActionResult CreateDefect()
        {
            Thread.Sleep(300); /* I wanna show my spinner ^^ */
            return PartialView("_CreateDefect");
        }

        public ActionResult CreateCar()
        {
            Thread.Sleep(200); /* I wanna show my spinner ^^ */
            return PartialView("_CreateCar");
        }

        // POST: SparePart/Create
        [HttpPost]
        public ActionResult Create(SparePartCreateEdit model, FormCollection collection)
        {
            ValidateCompositeKeyExistance(model);

            if (ModelState.IsValid)
            {
                var modelDTO = model.ToSparePartDTO();
                db.SpareParts.Create(modelDTO.ToSparePart());
                db.Save();
                return RedirectToAction("Index");
            }

            var modelOut = new SparePartCreateEdit(
                 db.Cars.GetAll().Select(x => x.ToCarDTO()).ToList(),
                db.Defects.GetAll().Select(x => x.ToDefectDTO()).ToList());

            return View("Create", modelOut);
        }

        // GET: SparePart/Edit/5
        public ActionResult Edit(int idCar, int idDefect)
        {
            var model = db.SpareParts.Find(x => x.IdCar == idCar && x.IdDefect == idDefect).FirstOrDefault();

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToSparePartDTO());
        }

        // POST: SparePart/Edit/5
        [HttpPost]
        public ActionResult Edit(SparePartDTO model, FormCollection collection)
        {
            var modelToUpdate = db.SpareParts.Find(x => x.IdCar == model.IdCar && x.IdDefect == model.IdDefect).FirstOrDefault();

            if (modelToUpdate == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {
                modelToUpdate.IdCar = model.IdCar;
                modelToUpdate.IdDefect = model.IdDefect;
                modelToUpdate.Name = model.Name;
                modelToUpdate.Price = model.Price;
                modelToUpdate.Amount = model.Amount;
                db.Save();

                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        // GET: SparePart/Delete/5
        public ActionResult Delete(int idCar, int idDefect)
        {
            var model = db.SpareParts.Find(x => x.IdCar == idCar && x.IdDefect == idDefect).FirstOrDefault();

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToSparePartDTO());
        }

        // POST: SparePart/Delete/5
        [HttpPost]
        public ActionResult Delete(int idCar, int idDefect, FormCollection collection)
        {
            var model = db.SpareParts.Find(x => x.IdCar == idCar && x.IdDefect == idDefect).FirstOrDefault();

            if (model == null)
                HttpNotFound();

            db.Context.SparePart.Remove(model);
            db.Save();
            return RedirectToAction("Index");
        }

        private void ValidateCompositeKeyExistance(SparePartCreateEdit model)
        {
            var sparePartAlreadyExisted = db.SpareParts.Find(x => x.IdCar == model.IdCar && x.IdDefect == model.IdDefect);

            if (sparePartAlreadyExisted != null && sparePartAlreadyExisted.Count()!=0)
            {
                ModelState.AddModelError("", Messages.SparePartCreate_CompositeKeyAlreadyExists);
            }
        }
    }
}
