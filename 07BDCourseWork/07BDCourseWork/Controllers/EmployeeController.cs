﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using _07BDCourseWork.Resources;
using _07BDCourseWork.ViewModels;

namespace _07BDCourseWork.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: Employee
        public ActionResult Index()
        {
            List<EmployeeDTO> model = db.Employees.GetAll().Select(item => item.ToEmployeeDTO()).ToList();
            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.EmployeeModel) as List<EmployeeDTO>;
            model = model ?? db.Employees.GetAll().Select(item => item.ToEmployeeDTO()).ToList();

            if (sortOrder == SortMethods.Name)
                return PartialView("_Table", model.OrderBy(x => x.Name).ToList());
            if (sortOrder == SortMethods.NameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Name).ToList());
            if (sortOrder == SortMethods.WorkshopName)
                return PartialView("_Table", model.OrderBy(x => x.Workshop.Name).ToList());
            if (sortOrder == SortMethods.WorkshopNameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Workshop.Name).ToList());
            if (sortOrder == SortMethods.BrigadeName)
                return PartialView("_Table", model.OrderBy(x => x.Brigade.Name).ToList());
            if (sortOrder == SortMethods.BrigadeNameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Brigade.Name).ToList());
            if (sortOrder == SortMethods.Inn)
                return PartialView("_Table", model.OrderBy(x => x.InnEmployee).ToList());
            if (sortOrder == SortMethods.InnDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.InnEmployee).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Filter(string name, string workshop, string brigade, string inn)
        {
            List<EmployeeDTO> model = db.Employees.GetAll().Select(item => item.ToEmployeeDTO()).ToList();

            model = model.Where(x => x.Name.ToString().StartsWith(name)).ToList();
            model = model.Where(x => x.Workshop.Name.ToString().StartsWith(workshop)).ToList();
            model = model.Where(x => x.Brigade.Name.ToString().StartsWith(brigade)).ToList();
            model = model.Where(x => x.InnEmployee.ToString().StartsWith(inn)).ToList();

            return PartialView("_Table", model);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            var model = db.Employees.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToEmployeeDTO());
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            var model = new EmployeeCreateEdit(
                db.Workshops.GetAll().Select(x => x.ToWorkshopDTO()).ToList(), 
                db.Brigades.GetAll().Select(x=>x.ToBrigadeDTO()).ToList());
            return View("Create", model);
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeCreateEdit model , FormCollection collection)
        {
            var modelDTO = model.ToEmployee();

            ValidateWorkshopExistance(modelDTO);
            ValidateBrigadeExistance(modelDTO);
            ValidateUniqueInn(modelDTO);

            if (ModelState.IsValid)
            {
                db.Employees.Create(modelDTO);
                db.Save();
                return RedirectToAction("Index");
            }
            return View("Create", model);
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            var model = db.Employees.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToEmployeeCreateEdit(db));
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(EmployeeCreateEdit model, FormCollection collection)
        {
            var modelToUpdate = db.Employees.Get(model.InnEmployee);

            if (modelToUpdate == null)
                return HttpNotFound();

            ValidateWorkshopExistance(model.ToEmployee());
            ValidateBrigadeExistance(model.ToEmployee());

            if (ModelState.IsValid)
            {
                modelToUpdate.Name = model.Name;
                modelToUpdate.IdWorkshop = model.IdWorkshop;
                modelToUpdate.IdBrigade = model.IdBrigade;

                db.Save();

                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            var model = db.Employees.Get(id);

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToEmployeeDTO());
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var model = db.Employees.Get(id);

            if (model == null)
                HttpNotFound();

            db.Employees.Delete(id);
            db.Save();
            return RedirectToAction("Index");
        }

        private void ValidateWorkshopExistance(Employee model)
        {
            if (db.Workshops.Get(model.IdWorkshop) == null)
            {
                ModelState.AddModelError("IdWorkshop", Messages.EmployeeShouldHaveAWorkshop);
                ViewBag.Error = Messages.EmployeeShouldHaveAWorkshop;
            }
        }

        private void ValidateBrigadeExistance(Employee model)
        {
            if (db.Brigades.Get(model.IdBrigade) == null)
            {
                ModelState.AddModelError("IdBrigade", Messages.EmployeeShouldHaveABrigade);
                ViewBag.Error = Messages.EmployeeShouldHaveABrigade;
            }
        }

        private void ValidateUniqueInn(Employee model)
        {
            if (db.Employees.Find(x=>x.InnEmployee == model.InnEmployee) != null && db.Employees.Find(x => x.InnEmployee == model.InnEmployee).Count() != 0)
            {
                ModelState.AddModelError("InnEmployee", Messages.EmployeeController_ValidateUniqueINN_Incorrect_INN__Employee_with_such_INN_already_exists);
                ViewBag.Error = Messages.EmployeeController_ValidateUniqueINN_Incorrect_INN__Employee_with_such_INN_already_exists;
            }
        }
    }
}
