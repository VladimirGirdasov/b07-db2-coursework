﻿using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Controllers
{
    public class CarController : Controller
    {
        private readonly EFUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: Car
        public ActionResult Index(string sortOrder)
        {
            List<CarDTO> model = db.Cars.GetAll().Select(item => item.ToCarDTO()).ToList();
            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.CarModel) as List<CarDTO>;
            model = model ?? db.Cars.GetAll().Select(item => item.ToCarDTO()).ToList();

            if (sortOrder == SortMethods.IdCarcase)
                return PartialView("_Table", model.OrderBy(x => x.IdCarcase).ToList());
            if (sortOrder == SortMethods.IdCarcaseDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.IdCarcase).ToList());
            if (sortOrder == SortMethods.IdEngine)
                return PartialView("_Table", model.OrderBy(x => x.IdEngine).ToList());
            if (sortOrder == SortMethods.IdEngineDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.IdEngine).ToList());
            if (sortOrder == SortMethods.Owner)
                return PartialView("_Table", model.OrderBy(x => x.Owner).ToList());
            if (sortOrder == SortMethods.OwnerDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Owner).ToList());
            if (sortOrder == SortMethods.FactoryNumber)
                return PartialView("_Table", model.OrderBy(x => x.FactoryNumber).ToList());
            if (sortOrder == SortMethods.FactoryNumberDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.FactoryNumber).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Filter(string carcase, string engine, string owner, string factory)
        {
            List<CarDTO> model = db.Cars.GetAll().Select(item => item.ToCarDTO()).ToList();

            model = model.Where(x => x.IdCarcase.ToString().StartsWith(carcase)).ToList();
            model = model.Where(x => x.IdEngine.ToString().StartsWith(engine)).ToList();
            model = model.Where(x => x.Owner.ToString().StartsWith(owner)).ToList();
            model = model.Where(x => x.FactoryNumber.ToString().StartsWith(factory)).ToList();

            return PartialView("_Table", model);
        }

        // GET: Car/Details/5
        public ActionResult Details(int id)
        {
            var model = db.Cars.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToCarDTO());
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            return View("Create");
        }

        // POST: Car/Create
        [HttpPost]
        public ActionResult Create(CarDTO model, FormCollection collection)
        {
            ValidateUniqueIdCarcase(model);
            ValidateUniqueFactoryNumber(model);
            ValidateUniqueIdEngine(model);

            if (ModelState.IsValid)
            {
                var modelDTO = model.ToCar();
                db.Cars.Create(modelDTO);
                db.Save();
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        // GET: Car/Edit/5
        public ActionResult Edit(int id)
        {
            var model = db.Cars.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToCarDTO());
        }

        // POST: Car/Edit/5
        [HttpPost]
        public ActionResult Edit(CarDTO model, FormCollection collection)
        {
            var modelToUpdate = db.Cars.Get(model.ID);

            if (modelToUpdate == null)
                return HttpNotFound();

            ValidateUniqueIdCarcase(model);
            ValidateUniqueFactoryNumber(model);
            ValidateUniqueIdEngine(model);

            if (ModelState.IsValid)
            {
                modelToUpdate.IdCarcase = model.IdCarcase;
                modelToUpdate.IdEngine = model.IdEngine;
                modelToUpdate.Owner = model.Owner;
                modelToUpdate.FactoryNumber = model.FactoryNumber;
                db.Save();

                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        // GET: Car/Delete/5
        public ActionResult Delete(int id)
        {
            var model = db.Cars.Get(id);

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToCarDTO());
        }

        // POST: Car/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var model = db.Cars.Get(id);

            if (model == null)
                HttpNotFound();

            //db.Cars.Delete(id);
            db.Context.Database.ExecuteSqlCommand($"DELETE FROM Car WHERE ID = {id}");
            db.Save();
            return RedirectToAction("Index");
        }

        private void ValidateUniqueIdCarcase(CarDTO model)
        {
            var existedSameName = db.Cars.Find(x => x.IdCarcase == model.IdCarcase && x.ID != model.ID);

            if (existedSameName != null && existedSameName.Count() != 0)
            {
                ModelState.AddModelError("IdCarcase", Messages.CarCreateValid_UniqNumber);
            }
        }

        private void ValidateUniqueIdEngine(CarDTO model)
        {
            var existedSameName = db.Cars.Find(x => x.IdEngine == model.IdEngine && x.ID != model.ID);

            if (existedSameName != null && existedSameName.Count() != 0)
            {
                ModelState.AddModelError("IdEngine", Messages.CarCreateValid_UniqNumber);
            }
        }

        private void ValidateUniqueFactoryNumber(CarDTO model)
        {
            var existedSameName = db.Cars.Find(x => x.FactoryNumber == model.FactoryNumber && x.ID != model.ID);

            if (existedSameName != null && existedSameName.Count() != 0)
            {
                ModelState.AddModelError("FactoryNumber", Messages.CarCreateValid_UniqNumber);
            }
        }
    }
}
