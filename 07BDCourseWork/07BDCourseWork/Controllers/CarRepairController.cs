﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using _07BDCourseWork.Resources;
using _07BDCourseWork.ViewModels;

namespace _07BDCourseWork.Controllers
{
    public class CarRepairController : Controller
    {
        private readonly IUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: CarRepair
        public ActionResult Index()
        {
            List<CarRepairDTO> model = db.CarRepairs.GetAll().Select(item => item.ToCarRepairDTO()).ToList();
            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.CarRepairModel) as List<CarRepairDTO>;
            model = model ?? db.CarRepairs.GetAll().Select(item => item.ToCarRepairDTO()).ToList();

            if (sortOrder == SortMethods.IdCarcase)
                return PartialView("_Table", model.OrderBy(x => x.Car.IdCarcase).ToList());
            if (sortOrder == SortMethods.IdCarcaseDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Car.IdCarcase).ToList());
            if (sortOrder == SortMethods.DefectName)
                return PartialView("_Table", model.OrderBy(x => x.Defect.Name).ToList());
            if (sortOrder == SortMethods.DefectNameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Defect.Name).ToList());
            if (sortOrder == SortMethods.DateIn)
                return PartialView("_Table", model.OrderBy(x => x.DateIn).ToList());
            if (sortOrder == SortMethods.DateInDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.DateIn).ToList());
            if (sortOrder == SortMethods.DateOut)
                return PartialView("_Table", model.OrderBy(x => x.DateOut).ToList());
            if (sortOrder == SortMethods.DateOutDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.DateOut).ToList());
            if (sortOrder == SortMethods.BrigadeName)
                return PartialView("_Table", model.OrderBy(x => x.Brigade.Name).ToList());
            if (sortOrder == SortMethods.BrigadeNameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Brigade.Name).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Filter(string carcase, string defect, string brigade, DateTime? dateinmin, DateTime? dateoutmax)
        {
            List<CarRepairDTO> model = db.CarRepairs.GetAll().Select(item => item.ToCarRepairDTO()).ToList();

            model = model.Where(x => x.Car.IdCarcase.ToString().StartsWith(carcase)).ToList();
            model = model.Where(x => x.Defect.Name.ToString().StartsWith(defect)).ToList();
            model = model.Where(x => x.Brigade.Name.ToString().StartsWith(brigade)).ToList();

            if (dateinmin != null)
                model = model.Where(x => x.DateIn >= dateinmin.Value).ToList();

            if (dateoutmax != null)
                model = model.Where(x => x.DateOut <= dateoutmax.Value).ToList();

            return PartialView("_Table", model);
        }

        // GET: CarRepair/Details/5
        public ActionResult Details(int id)
        {
            var model = db.CarRepairs.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToCarRepairDTO());
        }

        // GET: CarRepair/Create
        public ActionResult Create()
        {
            var model = new CarRepairCreateEdit(
                db.Cars.GetAll().Select(x=>x.ToCarDTO()).ToList(),
                db.Defects.GetAll().Select(x=>x.ToDefectDTO()).ToList(),
                db.Brigades.GetAll().Select(x=>x.ToBrigadeDTO()).ToList());

            model.DateIn = DateTime.Now;

            return View("Create", model);
        }

        // POST: CarRepair/Create
        [HttpPost]
        public ActionResult Create(CarRepairCreateEdit model, FormCollection collection)
        {
            var modelDTO = model.ToCarRepair();

            ValidateDate(modelDTO);
            ValidateDatesSequence(modelDTO);

            if (ModelState.IsValid)
            {
                db.CarRepairs.Create(modelDTO);
                db.Save();
                return RedirectToAction("Index");
            }

            var modelOut = new CarRepairCreateEdit(
                db.Cars.GetAll().Select(x => x.ToCarDTO()).ToList(),
                db.Defects.GetAll().Select(x => x.ToDefectDTO()).ToList(),
                db.Brigades.GetAll().Select(x => x.ToBrigadeDTO()).ToList());

            return View("Create", modelOut);
        }

        // GET: CarRepair/Edit/5
        public ActionResult Edit(int id)
        {
            var model = db.CarRepairs.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToCarRepairCreateEdit(db));
        }

        // POST: CarRepair/Edit/5
        [HttpPost]
        public ActionResult Edit(CarRepairCreateEdit model, FormCollection collection)
        {
            var modelToUpdate = db.CarRepairs.Get(model.ID);

            if (modelToUpdate == null)
                return HttpNotFound();

            ValidateDate(model.ToCarRepair());
            ValidateDatesSequence(model.ToCarRepair());

            if (ModelState.IsValid)
            {
                modelToUpdate.IdCar = model.IdCar;
                modelToUpdate.IdDefect = model.IdDefect;
                modelToUpdate.DateIn = model.DateIn;
                modelToUpdate.DateOut = model.DateOut;
                modelToUpdate.IdBrigade = model.IdBrigade;
                db.Save();

                return RedirectToAction("Index");
            }

            model.SelectListBrigade = db.Brigades.GetAll().Select(x => x.ToBrigadeDTO()).ToList();
            model.SelectListCar = db.Cars.GetAll().Select(x => x.ToCarDTO()).ToList();
            model.SelectListDefect = db.Defects.GetAll().Select(x => x.ToDefectDTO()).ToList();

            return View("Edit", model);
        }

        // GET: CarRepair/Delete/5
        public ActionResult Delete(int id)
        {
            var model = db.CarRepairs.Get(id);

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToCarRepairDTO());
        }

        // POST: CarRepair/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var model = db.CarRepairs.Get(id);

            if (model == null)
                HttpNotFound();

            db.CarRepairs.Delete(id);
            db.Save();
            return RedirectToAction("Index");
        }

        private void ValidateDate(CarRepair model)
        {
            if (model.DateIn.Year<1990 || model.DateIn > DateTime.Now)
            {
                ModelState.AddModelError("Datein", Messages.ValidateDate_Future_TooOld);
            }
            if (model.DateOut != null)
            {
                if (model.DateOut.Value.Year < 1990 || model.DateOut > DateTime.Now)
                {
                    ModelState.AddModelError("DateOut", Messages.ValidateDate_Future_TooOld);
                }
            }
        }

        private void ValidateDatesSequence(CarRepair model)
        {
            if (model.DateOut != null)
            {
                if (model.DateOut < model.DateIn)
                {
                    ModelState.AddModelError("DateOut", Messages.CarRepairCreateValid_SequenceDateInDateOutErr);
                }
            }
        }
    }
}
