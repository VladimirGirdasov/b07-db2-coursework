﻿using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using DAL.Interfaces;
using DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Controllers
{
    public class BrigadeController : Controller
    {
        private readonly IUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: Brigade
        public ActionResult Index()
        {
            List<BrigadeDTO> model = new List<BrigadeDTO>();
            foreach (var item in db.Brigades.GetAll())
            {
                model.Add(item.ToBrigadeDTO());
            }
            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.BrigadeModel) as List<BrigadeDTO>;
            model = model ?? db.Brigades.GetAll().Select(item => item.ToBrigadeDTO()).ToList();

            if (sortOrder == SortMethods.Name)
                return PartialView("_Table", model.OrderBy(x => x.Name).ToList());

            if (sortOrder == SortMethods.NameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Name).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            List<BrigadeDTO> model = db.Brigades.GetAll().Where(x => x.Name.StartsWith(name)).Select(item => item.ToBrigadeDTO()).ToList();
            return PartialView("_Table", model);
        }

        // GET: Brigade/Details/5
        public ActionResult Details(int id)
        {
            var model = db.Brigades.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToBrigadeDTO());
        }

        // GET: Brigade/Create
        public ActionResult Create()
        {
            return View("Create");
        }

        // POST: Brigade/Create
        [HttpPost]
        public ActionResult Create(BrigadeDTO model, FormCollection collection)
        {
            ValidateUniqueName(model);

            if (ModelState.IsValid)
            {
                var modelDTO = model.ToBrigade();
                db.Brigades.Create(modelDTO);
                db.Save();
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        // GET: Brigade/Edit/5
        public ActionResult Edit(int id)
        {
            var model = db.Brigades.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToBrigadeDTO());
        }

        // POST: Brigade/Edit/5
        [HttpPost]
        public ActionResult Edit(BrigadeDTO model, FormCollection collection)
        {
            var modelToUpdate = db.Brigades.Get(model.ID);

            if (modelToUpdate == null)
                return HttpNotFound();

            ValidateUniqueName(model);

            if (ModelState.IsValid)
            {
                modelToUpdate.Name = model.Name;
                db.Save();

                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        // GET: Brigade/Delete/5
        public ActionResult Delete(int id)
        {
            var model = db.Brigades.Get(id);

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToBrigadeDTO());
        }

        // POST: Brigade/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var model = db.Brigades.Get(id);

            if (model == null)
                HttpNotFound();

            db.Brigades.Delete(id);
            db.Save();
            return RedirectToAction("Index");
        }

        private void ValidateUniqueName(BrigadeDTO model)
        {
            var existedSameName = db.Brigades.Find(x => x.Name == model.Name && x.ID != model.ID);

            if (existedSameName != null && existedSameName.Count() != 0)
            {
                ModelState.AddModelError("Name", Messages.BrigadeCreateValid_UniqName);
            }
        }
    }
}
