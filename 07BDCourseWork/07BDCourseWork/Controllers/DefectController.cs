﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DAL.Interfaces;
using DAL.Repositories;
using _07BDCourseWork.App_Start;
using _07BDCourseWork.Models;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Controllers
{
    public class DefectController : Controller
    {
        private readonly IUnitOfWork db = new EFUnitOfWork("name=DbModel");

        // GET: Defect
        public ActionResult Index()
        {
            List<DefectDTO> model = db.Defects.GetAll().Select(item => item.ToDefectDTO()).ToList();

            if(model.Count != 0)
            {
                ViewBag.Min = (int)model.Min(x => x.Cost) - 1;
                ViewBag.Max = (int)model.Max(x => x.Cost) + 1;
            }

            return View("Index", model);
        }

        public ActionResult Sort(string sortOrder)
        {
            var model = CacheHelper.Get(CacheKeys.DefectModel) as List<DefectDTO>;
            model = model ?? db.Defects.GetAll().Select(item => item.ToDefectDTO()).ToList();

            if (sortOrder == SortMethods.Name)
                return PartialView("_Table", model.OrderBy(x => x.Name).ToList());
            if (sortOrder == SortMethods.NameDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Name).ToList());
            if (sortOrder == SortMethods.Cost)
                return PartialView("_Table", model.OrderBy(x => x.Cost).ToList());
            if (sortOrder == SortMethods.CostDesc)
                return PartialView("_Table", model.OrderByDescending(x => x.Cost).ToList());

            return PartialView("_table", model);
        }

        [HttpPost]
        public ActionResult Filter(string name, string mincost, string maxcost)
        {
            decimal min, max;

            if (!decimal.TryParse(mincost, out min) || !decimal.TryParse(maxcost, out max))
                return PartialView("_Table", new List<DefectDTO>());

            if (min > max)
                return PartialView("_Table", new List<DefectDTO>());

            List<DefectDTO> model = db.Defects.GetAll().Select(item => item.ToDefectDTO()).ToList();
            
            model = model.Where(x => x.Name.ToString().StartsWith(name)).ToList();
            model = model.Where(x => x.Cost >= min).ToList();
            model = model.Where(x => x.Cost <= max).ToList();

            return PartialView("_Table", model);
        }

        // GET: Defect/Details/5
        public ActionResult Details(int id)
        {
            var model = db.Defects.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Details", model.ToDefectDTO());
        }

        // GET: Defect/Create
        public ActionResult Create()
        {
            return View("Create");
        }

        // POST: Defect/Create
        [HttpPost]
        public ActionResult Create(DefectDTO model, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                var modelDTO = model.ToDefect();
                db.Defects.Create(modelDTO);
                db.Save();
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        // GET: Defect/Edit/5
        public ActionResult Edit(int id)
        {
            var model = db.Defects.Get(id);

            if (model == null)
                return HttpNotFound();

            return View("Edit", model.ToDefectDTO());
        }

        // POST: Defect/Edit/5
        [HttpPost]
        public ActionResult Edit(DefectDTO model, FormCollection collection)
        {
            var modelToUpdate = db.Defects.Get(model.ID);

            if (modelToUpdate == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {
                modelToUpdate.Name = model.Name;
                modelToUpdate.Cost = model.Cost;
                db.Save();

                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        // GET: Defect/Delete/5
        public ActionResult Delete(int id)
        {
            var model = db.Defects.Get(id);

            if (model == null)
                HttpNotFound();

            return View("Delete", model.ToDefectDTO());
        }

        // POST: Defect/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            var model = db.Defects.Get(id);

            if (model == null)
                HttpNotFound();

            db.Defects.Delete(id);
            db.Save();
            return RedirectToAction("Index");
        }
    }
}