﻿using System.Web.Mvc;

namespace _07BDCourseWork.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}