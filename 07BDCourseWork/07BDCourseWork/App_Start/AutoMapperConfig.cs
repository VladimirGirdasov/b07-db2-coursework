﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Entities;
using DAL.Interfaces;
using _07BDCourseWork.Models;
using _07BDCourseWork.ViewModels;

namespace _07BDCourseWork.App_Start
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration MapperConfiguration;

        public static void RegisterMappings()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Car, CarDTO>()
                .ForMember(x=>x.CarRepair, s=>s.Ignore())
                .ForMember(x => x.SparePart, s => s.Ignore())
                .AfterMap((s, d) =>
                    {
                        d.SparePart = new List<SparePartDTO>();
                        foreach (var item in s.SparePart)
                        {
                            d.SparePart.Add(new SparePartDTO
                            {
                                Name = item.Name,
                                IdCar = item.IdCar,
                                IdDefect = item.IdDefect,
                                Amount = item.Amount
                            });
                        }

                        d.CarRepair = new List<CarRepairDTO>();
                        foreach (var item in s.CarRepair)
                        {
                            d.CarRepair.Add(new CarRepairDTO
                            {
                                ID = item.ID,
                                DateIn = item.DateIn,
                                Car = new CarDTO
                                {
                                    ID = item.Car.ID,
                                    FactoryNumber = item.Car.FactoryNumber,
                                    IdCarcase = item.Car.IdCarcase,
                                    IdEngine = item.Car.IdEngine,
                                },
                                Defect = new DefectDTO
                                {
                                    ID = item.Defect.ID,
                                    Name = item.Defect.Name
                                }
                            });
                        }
                    });

                cfg.CreateMap<CarDTO, Car>();

                cfg.CreateMap<Brigade, BrigadeDTO>()
                    .ForMember(x => x.CarRepair, s => s.Ignore())
                    .ForMember(x => x.Employee, s => s.Ignore())
                    .AfterMap((s, d) =>
                    {
                        d.CarRepair = new List<CarRepairDTO>();
                        foreach (var item in s.CarRepair)
                        {
                            d.CarRepair.Add(new CarRepairDTO
                            {
                                ID = item.ID,
                                DateIn = item.DateIn,
                                Car = new CarDTO
                                {
                                    ID = item.Car.ID,
                                    FactoryNumber = item.Car.FactoryNumber,
                                    IdCarcase = item.Car.IdCarcase,
                                    IdEngine = item.Car.IdEngine,
                                },
                                Defect = new DefectDTO
                                {
                                    ID = item.Defect.ID,
                                    Name = item.Defect.Name
                                }
                            });
                        }

                        d.Employee = new List<EmployeeDTO>();
                        foreach (var item in s.Employee)
                        {
                            d.Employee.Add(new EmployeeDTO
                            {
                                InnEmployee = item.InnEmployee,
                                Name = item.Name
                            });
                        }
                    });

                cfg.CreateMap<BrigadeDTO, Brigade>();

                cfg.CreateMap<CarRepair, CarRepairCreateEdit>();

                cfg.CreateMap<CarRepair, CarRepairDTO>()
                    .ForMember(m => m.Car, src => src.Ignore())
                    .ForMember(m => m.Defect, src => src.Ignore())
                    .ForMember(m => m.Brigade, src => src.Ignore())
                    .AfterMap((s, d) =>
                    {
                        d.Defect = new DefectDTO
                        {
                            ID = s.Defect.ID,
                            Name = s.Defect.Name
                        };

                        d.Brigade = new BrigadeDTO
                        {
                            ID = s.Brigade.ID,
                            Name = s.Brigade.Name
                        };

                        d.Car = new CarDTO
                        {
                            ID = s.Car.ID,
                            FactoryNumber = s.Car.FactoryNumber,
                            IdCarcase = s.Car.IdCarcase,
                            IdEngine = s.Car.IdEngine,
                        };
                    });

                cfg.CreateMap<CarRepairCreateEdit, CarRepairDTO>();

                cfg.CreateMap<SparePartCreateEdit, SparePartDTO>();

                cfg.CreateMap<SparePart, SparePartCreateEdit>();
                
                cfg.CreateMap<CarRepairDTO, CarRepair>();

                cfg.CreateMap<Defect, DefectDTO>()
                    .ForMember(x => x.CarRepair, s => s.Ignore())
                    .ForMember(x => x.SparePart, s => s.Ignore())
                    .AfterMap((s, d) =>
                    {
                        d.CarRepair = new List<CarRepairDTO>();
                        foreach (var item in s.CarRepair)
                        {
                            d.CarRepair.Add(new CarRepairDTO
                            {
                                ID = item.ID,
                                DateIn = item.DateIn,
                                Car = new CarDTO
                                {
                                    ID = item.Car.ID,
                                    FactoryNumber = item.Car.FactoryNumber,
                                    IdCarcase = item.Car.IdCarcase,
                                    IdEngine = item.Car.IdEngine,
                                },
                                Defect = new DefectDTO
                                {
                                    ID = item.Defect.ID,
                                    Name = item.Defect.Name
                                }
                            });
                        }

                        d.SparePart = new List<SparePartDTO>();
                        foreach (var item in s.SparePart)
                        {
                            d.SparePart.Add(new SparePartDTO
                            {
                                Name = item.Name,
                                IdCar = item.IdCar,
                                IdDefect = item.IdDefect,
                                Amount = item.Amount
                            });
                        }
                    });

                cfg.CreateMap<DefectDTO, Defect>();

                cfg.CreateMap<EmployeeCreateEdit, Employee>();

                cfg.CreateMap<Employee, EmployeeCreateEdit>();

                cfg.CreateMap<Employee, EmployeeDTO>()
                    .ForMember(x => x.Workshop, s => s.Ignore())
                    .ForMember(x => x.Brigade, s => s.Ignore())
                    .AfterMap((s, d) =>
                    {
                        d.Workshop = new WorkshopDTO
                        {
                            ID = s.Workshop.ID,
                            Name = s.Workshop.Name
                        };
                        d.Brigade = new BrigadeDTO
                        {
                            ID = s.Brigade.ID,
                            Name = s.Brigade.Name
                        };
                    });

                cfg.CreateMap<EmployeeDTO, Employee>();

                cfg.CreateMap <SparePart, SparePartDTO> ()
                    .ForMember(m => m.Car, src => src.Ignore())
                    .ForMember(m => m.Defect, src => src.Ignore())
                    .AfterMap((s, d) =>
                    {
                        d.Car = new CarDTO
                        {
                            ID = s.Car.ID,
                            FactoryNumber = s.Car.FactoryNumber,
                            IdCarcase = s.Car.IdCarcase,
                            IdEngine = s.Car.IdEngine,
                        };

                        d.Defect = new DefectDTO
                        {
                            ID = s.Defect.ID,
                            Name = s.Defect.Name
                        };
                    });

                cfg.CreateMap<SparePartDTO, SparePart>();

                cfg.CreateMap<Workshop, WorkshopDTO>()
                .ForMember(x=>x.Employee, s=>s.Ignore())
                .AfterMap((s, d) =>
                    {
                        d.Employee = new List<EmployeeDTO>();
                        foreach (var item in s.Employee)
                        {
                            d.Employee.Add(new EmployeeDTO
                            {
                                InnEmployee = item.InnEmployee,
                                Name = item.Name
                            });
                        }
                    });

                cfg.CreateMap<WorkshopDTO, Workshop>();
            });
        }

        #region Car

        public static Car ToCar(this CarDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<CarDTO, Car>(entity);
        }

        public static CarDTO ToCarDTO(this Car entity)
        {
            return MapperConfiguration.CreateMapper().Map<Car, CarDTO>(entity);
        }

        #endregion
        
        #region Workshop

        public static Workshop ToWorkshop(this WorkshopDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<WorkshopDTO, Workshop>(entity);
        }

        public static WorkshopDTO ToWorkshopDTO(this Workshop entity)
        {
            return MapperConfiguration.CreateMapper().Map<Workshop, WorkshopDTO>(entity);
        }

        #endregion
        
        #region Brigade

        public static Brigade ToBrigade(this BrigadeDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<BrigadeDTO, Brigade>(entity);
        }

        public static BrigadeDTO ToBrigadeDTO(this Brigade entity)
        {
            return MapperConfiguration.CreateMapper().Map<Brigade, BrigadeDTO>(entity);
        }

        #endregion
        
        #region CarReapair

        public static CarRepair ToCarRepair(this CarRepairDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<CarRepairDTO, CarRepair>(entity);
        }

        public static CarRepairDTO ToCarRepairDTO(this CarRepair entity)
        {
            return MapperConfiguration.CreateMapper().Map<CarRepair, CarRepairDTO>(entity);
        }

        public static CarRepairCreateEdit ToCarRepairCreateEdit(this CarRepair entity, IUnitOfWork db)
        {
            var ret = MapperConfiguration.CreateMapper().Map<CarRepair, CarRepairCreateEdit>(entity);

            ret.SelectListBrigade = db.Brigades.GetAll().Select(x => x.ToBrigadeDTO()).ToList();
            ret.SelectListCar = db.Cars.GetAll().Select(x => x.ToCarDTO()).ToList();
            ret.SelectListDefect = db.Defects.GetAll().Select(x => x.ToDefectDTO()).ToList();

            return ret;
        }

        public static CarRepairDTO ToCarRepairDTO(this CarRepairCreateEdit entity)
        {
            return MapperConfiguration.CreateMapper().Map<CarRepairCreateEdit, CarRepairDTO>(entity);
        }

        #endregion
        
        #region Defect

        public static Defect ToDefect(this DefectDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<DefectDTO, Defect>(entity);
        }

        public static DefectDTO ToDefectDTO(this Defect entity)
        {
            return MapperConfiguration.CreateMapper().Map<Defect, DefectDTO>(entity);
        }

        #endregion
        
        #region Employee

        public static Employee ToEmployee(this EmployeeDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<EmployeeDTO, Employee>(entity);
        }

        public static EmployeeDTO ToEmployeeDTO(this Employee entity)
        {
            return MapperConfiguration.CreateMapper().Map<Employee, EmployeeDTO>(entity);
        }

        public static Employee ToEmployee(this EmployeeCreateEdit entity)
        {
            return MapperConfiguration.CreateMapper().Map<EmployeeCreateEdit, Employee>(entity);
        }

        public static EmployeeCreateEdit ToEmployeeCreateEdit(this Employee entity, IUnitOfWork db)
        {
            var ret = MapperConfiguration.CreateMapper().Map<Employee, EmployeeCreateEdit>(entity);

            ret.SelectListWorkshop = db.Workshops.GetAll().Select(x => x.ToWorkshopDTO()).ToList();
            ret.SelectListBrigade = db.Brigades.GetAll().Select(x => x.ToBrigadeDTO()).ToList();

            return ret;
        }

        #endregion

        #region SparePart

        public static SparePart ToSparePart(this SparePartDTO entity)
        {
            return MapperConfiguration.CreateMapper().Map<SparePartDTO, SparePart>(entity);
        }

        public static SparePartDTO ToSparePartDTO(this SparePart entity)
        {
            return MapperConfiguration.CreateMapper().Map<SparePart, SparePartDTO>(entity);
        }

        public static SparePartDTO ToSparePartDTO(this SparePartCreateEdit entity)
        {
            return MapperConfiguration.CreateMapper().Map<SparePartCreateEdit, SparePartDTO>(entity);
        }

        public static SparePartCreateEdit ToSparePartCreateEdit(this SparePart entity, IUnitOfWork db)
        {
            var ret = MapperConfiguration.CreateMapper().Map<SparePart, SparePartCreateEdit>(entity);

            ret.SelectListCar = db.Cars.GetAll().Select(x => x.ToCarDTO()).ToList();
            ret.SelectListDefect = db.Defects.GetAll().Select(x => x.ToDefectDTO()).ToList();

            return ret;
        }

        #endregion
    }
}