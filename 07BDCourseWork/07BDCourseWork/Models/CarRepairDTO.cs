﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _07BDCourseWork.Models
{
    public class CarRepairDTO
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int IdCar { get; set; }

        [Required]
        public int IdDefect { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateIn { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateOut { get; set; }

        public int? IdBrigade { get; set; }

        public BrigadeDTO Brigade { get; set; }

        public CarDTO Car { get; set; }

        public DefectDTO Defect { get; set; }
    }
}