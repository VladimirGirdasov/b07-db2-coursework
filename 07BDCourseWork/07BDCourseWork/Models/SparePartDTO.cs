﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _07BDCourseWork.Models
{
    public class SparePartDTO
    {
        [Key]
        public int IdCar { get; set; }

        [Key]
        public int IdDefect { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.01, int.MaxValue)]
        public decimal Price { get; set; }

        [Range(1, int.MaxValue)]
        public int Amount { get; set; }

        public CarDTO Car { get; set; }

        public DefectDTO Defect { get; set; }
    }
}