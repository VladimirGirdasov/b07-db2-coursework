﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Models
{
    public class DefectDTO
    {
        public DefectDTO()
        {
            CarRepair = new List<CarRepairDTO>();
            SparePart = new List<SparePartDTO>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z _]*$", ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "DefectNameWrongSymbols")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Range(0.01, int.MaxValue)]
        public decimal Cost { get; set; }

        public ICollection<CarRepairDTO> CarRepair { get; set; }

        public ICollection<SparePartDTO> SparePart { get; set; }
    }
}