﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Models
{
    public class WorkshopDTO
    {
        public WorkshopDTO()
        {
            Employee = new List<EmployeeDTO>();
        }

        [Key]
        public int ID { get; set; }

        [RegularExpression("^[a-zA-Z0-9 _-]*$", ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "WorkshopNameWrongSymbols")]
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public ICollection<EmployeeDTO> Employee { get; set; }
    }
}