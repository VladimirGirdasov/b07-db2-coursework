﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Models
{
    public class EmployeeDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Range(1, int.MaxValue)]
        [DisplayName(@"INN")]
        public int InnEmployee { get; set; }

        public int IdWorkshop { get; set; }

        public int IdBrigade { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z _]*$", ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "EmployeeNameWrongSymbols")]
        public string Name { get; set; }

        public BrigadeDTO Brigade { get; set; }

        public WorkshopDTO Workshop { get; set; }
    }
}