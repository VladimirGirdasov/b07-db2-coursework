﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Models
{
    public class CarDTO
    {
        public CarDTO()
        {
            CarRepair = new List<CarRepairDTO>();
            SparePart = new List<SparePartDTO>();
        }

        public int ID { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        [DisplayName(@"Carcase №")]
        public int IdCarcase { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        [DisplayName(@"Engine №")]
        public int IdEngine { get; set; }

        [StringLength(50)]
        [RegularExpression("^[a-zA-Z _]*$", ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "CarOwnerNameWrongSymbols")]
        [Required]
        public string Owner { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        [DisplayName(@"Factory №")]
        public int FactoryNumber { get; set; }

        public ICollection<CarRepairDTO> CarRepair { get; set; }

        public ICollection<SparePartDTO> SparePart { get; set; }
    }
}