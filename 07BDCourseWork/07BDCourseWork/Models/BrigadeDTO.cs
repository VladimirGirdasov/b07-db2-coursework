﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using _07BDCourseWork.Resources;

namespace _07BDCourseWork.Models
{
    public class BrigadeDTO
    {
        public BrigadeDTO()
        {
            CarRepair = new List<CarRepairDTO>();
            Employee = new List<EmployeeDTO>();
        }
        
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z0-9 _-]*$", ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "BrigadeNameWrongSymbols")]
        public string Name { get; set; }

        public ICollection<CarRepairDTO> CarRepair { get; set; }

        public ICollection<EmployeeDTO> Employee { get; set; }
    }
}